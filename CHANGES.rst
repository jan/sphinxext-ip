Changes
=======

0.6.1 - 2023-01-29
------------------

* reupload to include namespace package

0.6.0 - 2023-01-29
------------------

* add development documentation in development.rst
* use tox as test runner
* add type annotations
* fix Sphinx 6 deprecation warnings

0.5.1 - 2021-09-04
------------------

* fix anchors in links

0.5.0 - 2021-09-04
------------------

* fix Docutils error
* adapt to Sphinx 4

0.4.0 - 2021-01-02
------------------

* fix Docutils deprecation warning
* fix test deprecation warning from Sphinx
* use pytest for testing
* switch to PEP-517 build API and metadata

0.3.0 - 2019-07-13
------------------

* update to Sphinx 2.1 API

0.2.4 - 2016-05-07
------------------

* fix index generation issue

0.2.3 - 2016-05-05
------------------

* fix regression in HTML builder that has been introduced in 0.2.2

0.2.2 - 2016-05-05
------------------

* fix index entries that broke the latex builder

0.2.1 - 2016-05-05
------------------

* fix handling of invalid IP address/range values
* implement a proper clear_doc method for the ip domain

0.2.0 - 2016-05-04
------------------

* display IP address lists as tables
* sort IP address lists numericaly
