==================
jandd.sphinxext.ip
==================

This is an IP address extension for `Sphinx`_. The extension provides a domain
*ip* that allows marking IPv4 and IPv6 addresses in documentation and contains
directives to collect information regarding IP addresses in IP ranges.

.. _Sphinx: http://www.sphinx-doc.org/

Contributors
============

* `Jan Dittberner`_

.. _Jan Dittberner: https://jan.dittberner.info/
