.. Sphinxext IP Tests documentation master file, created by
   sphinx-quickstart on Sun May  1 18:04:18 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sphinxext IP Tests's documentation!
==============================================

Contents:

.. toctree::
   :maxdepth: 2

   testpage1
   testpage2
   testpage3


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

